﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;
public enum aiPosition
{
    top,
    mid,
    bottom
}

public class CreepsSpawning : MonoBehaviour
{
    //this link to the spawn manager components
    public SpawnManager spawnManager;

    public UnityEvent spawn;
    public bool activateSecondLevel;
    public GameObject[] creeppool;

    public GameObject[] currentCreepSpawn; //creeps to spawn

    public int totalSpawned; //manages the total spawned creeps
    public int spawnAmount; //managed the amount
    public int waveSpawn; //manages the max 10th wave
    public GameObject[] towerList;

    public float timer = 30.0f;
    public aiPosition position;

    // Start is called before the first frame update
    void Start()
    {
        spawn.AddListener(UponDestroy);
        currentCreepSpawn = spawnManager.generalCreepSpawn;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            ResetTimer();
            SpawnEnemy(this.GetComponent<Fraction>().userFraction, this.gameObject);
        }
    }

    void UponDestroy()
    {

    }

    void ResetTimer()
    {
        timer = spawnManager.spawnInterval;
    }

    void SpawnEnemy(charFraction fraction, GameObject spawner)
    {
        spawnAmount++;
        totalSpawned++;
        if (spawnAmount >= waveSpawn)
        {
            if (activateSecondLevel == true)
            {
                currentCreepSpawn = spawnManager.superSiegeCreepSpawn;
            }

            else
            {
                currentCreepSpawn = spawnManager.siegeCreepSpawn;
            }
            //resets to zero
            spawnAmount = 0;
        }

        //start spawning creeps
        for (int i = 0; i < currentCreepSpawn.Length; i++)
        {
            GameObject newCreep = Instantiate(currentCreepSpawn[i]);
            newCreep.GetComponent<Fraction>().setFraction(fraction);
            newCreep.GetComponent<Fraction>().setFractionColor();

            //set AI Targeting Lists
            newCreep.GetComponent<CreepsAI>().setupTowerTargetList(towerList);
            newCreep.gameObject.transform.position = spawner.gameObject.transform.position;
        }

        //set next batch of creeps;
        SetNextCreepSpawn();

    }

    void SetNextCreepSpawn()
    {
        if (activateSecondLevel == true)
        {
            currentCreepSpawn = spawnManager.superCreepSpawn;
        }

        else
        {
            currentCreepSpawn = spawnManager.siegeCreepSpawn;
        }
    }
}
