﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public float CurrentHealth { get => health; }
    public float MaxHealth { get => maxHealth; }

    [SerializeField]
    private float health;
    [SerializeField]
    private float maxHealth;

    public bool invunable; //use this if the enemy/target cannot be killed

    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void RevivePlayer()
    {
        health = maxHealth;
    }

    public void Damage(float amount)
    {
        if (invunable != true)
        {
            health -= amount;
        }

        if (health <= 0 && this.gameObject.tag == "creep")
        {
            Death();
            //Debug.Log("Creep death");
        }

        if (health <= 0 && this.gameObject.tag == "tower")
        {
            Death();
            Debug.Log("Tower death");
        }

        if (health <= 0 && this.gameObject.tag == "player")
        {
            Despawn();
            Debug.Log("Player death");
        }
    }
    public void Heal(float amount)
    {
        health += amount;

    }

    //for creep,monster, etc... use
    public void Death()
    {
        //run expirence system
        //this.gameObject.GetComponent<UserExpirence>().attacker.gainEXP(this.gameObject.GetComponent<UserExpirence>().sendEXP());
        this.gameObject.transform.position = new Vector3(9999, 9999, 9999);
        Destroy(this.gameObject);
    }

    //for hero use
    public void Despawn()
    {
        this.gameObject.GetComponent<Respawn>().beginDespawning();
    }

    public float GetNormalizedHealth()
    {
        return health / maxHealth;
    }
}
