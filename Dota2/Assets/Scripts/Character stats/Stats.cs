﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public float armor;
    public float magicResistance;
    public float evasion;

    public float healthRegenSpeed;
    public float manaRegenSpeed;

    public float damage;
}
