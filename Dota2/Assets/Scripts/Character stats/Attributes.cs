﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MainAttribute
{
    Strength, Agility, Intelligence
}

public class Attributes : MonoBehaviour
{
    //Base Attributes
    public float Strength;
    public float Agility;
    public float Intelligence;
    public MainAttribute mainAttribute;

    //Attribute Modifiers
    [HideInInspector] public float StrengthModifier = 0;
    [HideInInspector] public float AgilityModifier = 0;
    [HideInInspector] public float IntelligenceModifier = 0;

    //Total Attribute Values
    public float TotalStrength { get => Strength + StrengthModifier; }
    public float TotalAgility {get => Agility + AgilityModifier; }
    public float TotalIntelligence {get => Intelligence + IntelligenceModifier; }

    private Stats stats;

    private void Start()
    {
        stats = GetComponent<Stats>();

        //Temporary values
        stats.armor += TotalAgility;
        stats.magicResistance += TotalIntelligence / 2;
        stats.healthRegenSpeed += TotalStrength;
        stats.manaRegenSpeed += TotalIntelligence;

        //Modify damage based on hero's main attribute
        switch (mainAttribute)
        {
            default:
                return;
            case MainAttribute.Strength:
                stats.damage += TotalStrength;
                return;
            case MainAttribute.Agility:
                stats.damage += TotalAgility;
                return;
            case MainAttribute.Intelligence:
                stats.damage += TotalIntelligence;
                return;
        }
    }
}
