﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mana : MonoBehaviour
{
    public float CurrentMana { get => mana; }
    public float MaxMana { get => maxMana; }

    [SerializeField]
    private float mana;
    [SerializeField]
    private float maxMana;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void gainMana(float amount)
    {
        mana += amount;
    }

    public void deductMana(float amount)
    {
        mana -= amount;
        if (mana < 0)
        {
            Debug.Log("Mana: mana is on negative numeric");
        }
    }

    public bool useMana(float requiredAmount)
    {
        if (mana >= requiredAmount)
        {
            deductMana(requiredAmount);
            return true;
        }
        else
        {
            return false;
        }
    }

    public float GetNormalizedMana()
    {
        return mana / maxMana;
    }
}
