﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
public class Respawn : MonoBehaviour
{
    public Transform respawnPoint;
    public float timer;
    public bool respawning;
    public UnityEvent externalEventsforRespawning;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (respawning == true)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                beginRespawning();
            }
        }
    }

    public void setRespawingTimer(float time)
    {
        timer = time;
        respawning = true;
    }

    public void beginDespawning()
    {
        disableAllFunctions();
        setRespawingTimer(this.gameObject.GetComponent<UserExpirence>().getRespawnTime());
        //move the playter to a new position
        this.gameObject.transform.position = respawnPoint.transform.position;

    }
    public void beginRespawning()
    {
        enableAllFunctions();
        this.gameObject.GetComponent<Health>().RevivePlayer();
        this.gameObject.transform.position = respawnPoint.transform.position;
        externalEventsforRespawning.Invoke();
        respawning = false;
    }

    void disableAllFunctions()
    {
        this.gameObject.GetComponent<MeshRenderer>().enabled = false;
        this.gameObject.GetComponent<CapsuleCollider>().enabled = false;
        this.gameObject.GetComponent<SphereCollider>().enabled = false;
        this.gameObject.GetComponent<CharacterMovement>().enabled = false;
        this.gameObject.GetComponent<MeleeAttack>().enabled = false;
        this.gameObject.GetComponent<NavMeshAgent>().enabled = false;
    }

    void enableAllFunctions()
    {
        this.gameObject.GetComponent<MeshRenderer>().enabled = true;
        this.gameObject.GetComponent<CapsuleCollider>().enabled = true;
        this.gameObject.GetComponent<SphereCollider>().enabled = true;
        this.gameObject.GetComponent<CharacterMovement>().enabled = true;
        this.gameObject.GetComponent<MeleeAttack>().enabled = true;
        this.gameObject.GetComponent<NavMeshAgent>().enabled = true;
    }
}
