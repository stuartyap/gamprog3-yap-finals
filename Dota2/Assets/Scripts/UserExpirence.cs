﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserExpirence : MonoBehaviour
{
    public int level = 1;
    public int currentEXP = 0;
    public int nextEXP = 0; //UI code
    public float respawnTime = 6;
    public levelList[] userLevelList;
    public ExpirenceController expController;

    public UserExpirence attacker;
    // Start is called before the first frame update
    void Start()
    {
        userLevelList = expController.levelLists; //setup expirence list
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //call this every kill
    public void gainEXP(int expirence)
    {
        currentEXP += expirence;

        //check if level up
        if (currentEXP >= userLevelList[level + 1].requiredXP)
        {
            levelUP();
        }
    }

    //for hero to hero exchange
    public int sendEXP()
    {
        return userLevelList[level].xpBounty;
    }

    public void levelUP()
    {
        //repeat the process until reached the new level
        while (currentEXP >= userLevelList[level + 1].requiredXP)
        {
            level++;
            respawnTime = userLevelList[level].respawnTime;
            nextEXP = userLevelList[level].requiredXP;
        }
    }

    public float getRespawnTime()
    {
        return respawnTime;
    }
}
