﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Manages incoming buffs on the unit's stats, health, or mana
public class BuffReceiver : MonoBehaviour
{
    private List<Buff> buffs;
    private Dictionary<string, Buff> buffDirectory;

    public Health Health { get; private set; }
    public Mana Mana { get; private set; }
    public Stats Stats { get; private set; }

    void Start()
    {
        //Initialize values
        buffs = new List<Buff>();
        buffDirectory = new Dictionary<string, Buff>();

        //Get references
        Health = GetComponent<Health>();
        Mana = GetComponent<Mana>();
        Stats = GetComponent<Stats>();
    }

    void Update()
    {
        foreach (Buff b in buffs)
        {
            if (b.InfiniteDuration) continue;

            else
            {
                b.Lifetime += Time.deltaTime;
                if (b.Lifetime >= b.Duration) RemoveBuff(b);
            }
        }
    }

    public void ApplyNewBuff(Buff newBuff)
    {
        //If the new buff exists in the current list
        if (buffDirectory.ContainsKey(newBuff.BuffName))
        {
            switch (newBuff.StackType)
            {
                default:
                    return;

                case BuffStackType.EffectStackable:
                    buffs.Add(newBuff); //Add the new buff and start it like normal
                    newBuff.StartBuff(this);
                    StartCoroutine(TickBuff(newBuff));
                    buffDirectory[newBuff.BuffName].CurrentStacks++; //Increase existing buff's stacks
                    return;

                case BuffStackType.EffectRefresh:
                    buffDirectory[newBuff.BuffName].Lifetime = 0; //Reset existing buff's lifetime to 0
                    Destroy(newBuff);
                    return;

                case BuffStackType.NotStackable:
                    Destroy(newBuff);
                    return;
            }
        }

        //If the new buff is not in the current list
        else
        {
            buffs.Add(newBuff);
            buffDirectory.Add(newBuff.BuffName, newBuff);

            newBuff.StartBuff(this);
            StartCoroutine(TickBuff(newBuff));
        }

    }

    //Making it public because some buffs can be removed by outside sources (dispel, death, etc.)
    public void RemoveBuff(Buff buff)
    {
        buffs.Remove(buff);
        if (buffDirectory[buff.BuffName].CurrentStacks <= 1) buffDirectory.Remove(buff.BuffName);
        else buffDirectory[buff.BuffName].CurrentStacks--;

        Destroy(buff);
    }

    private IEnumerator TickBuff(Buff buff)
    {
        yield return new WaitForSeconds(buff.BuffInterval);
        buff.UpdateBuff(this);

        if (buff != null) StartCoroutine(TickBuff(buff));
    }
}
