﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Buff/Armor Boost")]
public class Buff_ArmorBoost : Buff
{
    public float ArmorBonus = 1;

    protected override void OnBuffStart(BuffReceiver target)
    {
        target.Stats.armor += ArmorBonus;
    }

    protected override void OnBuffEnd(BuffReceiver target)
    {
        target.Stats.armor -= ArmorBonus;
    }
}
