﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum creepAIStatus
{
    moving,
    attacking
}
public class CreepsAI : MonoBehaviour
{
    public GameObject target; //current target
    public GameObject[] towers; //stores tower list
    public int nextTowerId = -1;
    creepAIStatus status;
    public NavMeshAgent creepsAgent;

    // Start is called before the first frame update
    void Start()
    {
        creepsAgent = this.GetComponent<NavMeshAgent>();
        target = null;
    }

    // Update is called once per frame
    void Update()
    {
        //set target to the next tower in the list
        if (target == null)
        {
            status = creepAIStatus.moving;
            switchTower();
        }

        //start attacking target
        if (status == creepAIStatus.attacking)
        {
            GetComponent<AttackComponent>().Attack(target.GetComponent<Health>());
        }

        ////moving back to tower
        //if (status == creepAIStatus.moving)
        //{
        //    switchTower();
        //}
    }

    //Checks if any unit enters the creep's collider range
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Fraction>() == null) return;

        //Attack the object if it is not an ally
        if (other.GetComponent<Fraction>().userFraction != this.GetComponent<Fraction>().userFraction)
        {
            status = creepAIStatus.attacking;
            target = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Switch target to null if the target left the creep's range
        if (target == other.gameObject)
        {
            target = null;
            //status = creepAIStatus.moving;
        }
    }

    //move onto the next tower
    void switchTower()
    {
        if (nextTowerId != towers.Length)
        {
            nextTowerId++;
            switchTargettoTower();
            creepsAgent.SetDestination(target.transform.position);
        }
        
    }

    void switchTargettoTower()
    {
        target = towers[nextTowerId];
    }
    //this is where the enemy would go to a specific tower
    void moveEnemyToTower()
    {
        creepsAgent.SetDestination(towers[nextTowerId].transform.position);
    }

    //this is where to setup the tower list
    public void setupTowerTargetList(GameObject[] list)
    {
        charFraction fraction = this.GetComponent<Fraction>().userFraction;

        for (int i = 0; i < list.Length; i++)
        {
            towers[i] = list[i];
        }
    }

    void attackTarget()
    {

    }
}
