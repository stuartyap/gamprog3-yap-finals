﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class levelList
{
    public int level;
    public int requiredXP;
    public int respawnTime;
    public int xpBounty;
}

public class ExpirenceController : MonoBehaviour
{
    //created this because doing this to each and every hero would take YEARS!
    public levelList[] levelLists;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
