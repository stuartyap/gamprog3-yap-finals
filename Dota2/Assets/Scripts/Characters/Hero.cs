﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : Character
{
    public string HeroName;

    public Attributes attributes { get; private set; }
    public CharacterMovement movement { get; private set; }

    protected override void Start()
    {
        base.Start();
        attributes = GetComponent<Attributes>();
        movement = GetComponent<CharacterMovement>();
    }
}
