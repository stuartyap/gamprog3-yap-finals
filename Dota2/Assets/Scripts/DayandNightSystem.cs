﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum timeofday
{
    day,
    night
}
public class DayandNightSystem : MonoBehaviour
{
    public Clocksystemv2 clock;
    public timeofday tod;

    public GameObject Sun;
    //clock setup
    public float daytimeDegree = 90.0f;
    public float nighttimeDegree = 90.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        //this is to setup the system for skill use.
        switch (clock.hours)
        {
            case 5:
                tod = timeofday.night;
                break;
            case 0:
                tod = timeofday.day;
                break;
            default:
                break;
        }

        SunRotation();


    }

    void SunRotation()
    {
        Sun.gameObject.transform.eulerAngles = new Vector3(calculateCurrentTime(),0,0);
    }

    public float calculateCurrentTime()
    {
        float rotateValue;
        float getMaxTime = clock.maxTime;
        float currentTime = clock.hours;

        //get the percentage of the clock
        float currentPercentage = (currentTime / getMaxTime) * 100;
        //finalized the value;
        rotateValue = 360 * (currentPercentage / 100);
        return rotateValue;
    }
}
