﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("Initial Values")]
    public Vector3 ActorDistance = new Vector3(0, 10, -10);
    /// <summary>
    /// Camera's starting angle on load
    /// </summary>
    public Vector3 InitialAngle = new Vector3(45, 0, 0);
    public Transform ActorToFollow;

    [Header("Camera Movement")]
    public float CameraKeyboardSpeed = 3; //Camera speed for using the arrow keys
    public float CameraMouseSpeed = 25; //Camera speed for using the mouse to look around
    public KeyCode CenterActorKey = KeyCode.F1;

    private Camera cameraRef;

    [Header("Camera Limiter")]
    public float minX = -75, maxX = 75, minZ = -75, maxZ = 75;
    void Start()
    {
        //Set camera position (set to (0,0,0) if there is no actor to follow)
        if (ActorToFollow == null) this.transform.position = Vector3.zero;
        else CenterToActor();

        cameraRef = GetComponent<Camera>();
    }
    
    void Update()
    {
        //Mouse input
        UpdateMouseMovement(10, CameraMouseSpeed);
        //Keyboard input
        UpdateKeyboardMovement(Input.GetAxis("Horizontal") * CameraKeyboardSpeed, 
                                Input.GetAxis("Vertical") * CameraKeyboardSpeed);
        //Scroll input
        UpdateZoom(Input.GetAxis("Mouse ScrollWheel"));

        if (Input.GetKeyDown(CenterActorKey)) CenterToActor();

        camLimiter();
    }

    /// <summary>
    /// Makes the camera focus on the actor
    /// </summary>
    public void CenterToActor()
    {
        if (ActorToFollow == null) return;

        this.transform.position = ActorToFollow.position + ActorDistance;
        this.transform.rotation = Quaternion.Euler(InitialAngle);
    }

    /// <summary>
    /// Updates the distance between the camera and its actor
    /// </summary>
    /// <param name="scrollValue"></param>
    void UpdateZoom(float scrollValue)
    {
        //Return if the updated y position goes too far or too close from the actor
        if (this.transform.position.y + scrollValue >= ActorToFollow.position.y + ActorDistance.y 
            || this.transform.position.y + scrollValue <= ActorToFollow.position.y) return;

        this.transform.position = this.transform.position + (Vector3.up * scrollValue);
    }

    /// <summary>
    /// Updates the camera position based on keyboard input
    /// </summary>
    /// <param name="xUpdate"></param>
    /// <param name="zUpdate"></param>
    void UpdateKeyboardMovement(float xUpdate, float zUpdate)
    {
        this.transform.position = this.transform.position + (Vector3.right * xUpdate);
        this.transform.position = this.transform.position + (Vector3.forward * zUpdate);
    }

    /// <summary>
    /// Updates the camera position based on mouse position
    /// </summary>
    /// <param name="offset"></param>
    /// <param name="speed"></param>
    void UpdateMouseMovement(float offset, float speed)
    {
        Vector3 update = Vector3.zero;

        if (Input.mousePosition.x < offset) update.x -= speed * Time.deltaTime;
        else if (Input.mousePosition.x > Screen.width - offset) update.x += speed * Time.deltaTime;

        if (Input.mousePosition.y < offset) update.z -= speed * Time.deltaTime;
        else if (Input.mousePosition.y > Screen.height - offset) update.z += speed * Time.deltaTime;

        this.transform.position = this.transform.position + update;
    }

    void camLimiter()
    {
        //xPosition
        Vector3 currentPosition = this.transform.position;
        currentPosition.x = Mathf.Clamp(currentPosition.x, minX, maxX);
        transform.position = currentPosition;
        //zPosition
        currentPosition.z = Mathf.Clamp(currentPosition.z, minZ, maxZ);
        transform.position = currentPosition;
    }
}
