﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ancient : Structure
{
    public int invunablelock = 2;

    private void Start()
    {
        health = GetComponent<Health>();
    }

    public void removeSpecialInvunability()
    {
        invunablelock--;

        if (invunablelock <= 0)
        {
            this.RemoveVulnerability();
        }
    }
}
