﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsHUD : MonoBehaviour
{
    [Header("Player References")]
    public Stats statsRef;
    public Attributes attrRef;

    [Header("HUD References")]
    [SerializeField] public Text DmgText;
    [SerializeField] public Text StrText;
    [SerializeField] public Text AgiText;
    [SerializeField] public Text IntText;
    [SerializeField] public Text ArmorText;
    [SerializeField] public Text SpdText;

    //void Start()
    //{
        
    //}

    void Update()
    {
        DmgText.text = statsRef.damage.ToString();
        StrText.text = "Str: " + attrRef.Strength + "+" + attrRef.StrengthModifier;
        AgiText.text = "Agi: " + attrRef.Agility + "+" + attrRef.AgilityModifier;
        IntText.text = "Int: " + attrRef.Intelligence + "+" + attrRef.IntelligenceModifier;
        ArmorText.text = "Armor: " + statsRef.armor.ToString();
    }
}
