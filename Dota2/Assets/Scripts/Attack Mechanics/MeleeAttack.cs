﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : AttackComponent
{
    /// <summary>
    /// Any unit inside the collider will take damage on attack
    /// </summary>
    public Collider AttackCollider;

    private void Start()
    {
        //collider only becomes active when the unit attacks
        AttackCollider.enabled = false;

        //collider should be a trigger because a solid collision might cause physics problems
        if (!AttackCollider.isTrigger) AttackCollider.isTrigger = true;
    }

    protected override void StartAttack()
    {
        base.StartAttack();
        StartCoroutine(ToggleCollider());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Health>() == target) other.GetComponent<Health>().Damage(Damage);
    }

    //Switches the collider on for a small time frame
    private IEnumerator ToggleCollider()
    {
        AttackCollider.enabled = true;

        yield return new WaitForSeconds(0.1f);

        AttackCollider.enabled = false;
    }
}
