﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Base class for auto attacks (melee and ranged)
public class AttackComponent : MonoBehaviour
{
    public float Damage;
    public float BaseAttackTime;

    protected Health target;

    private bool canAttack = true;

    public void Attack(Health attackTarget)
    {
        if (!canAttack) return;

        target = attackTarget;

        StartAttack();
        StartCoroutine(AttackCooldown());
    }

    protected virtual void StartAttack() { }

    private IEnumerator AttackCooldown()
    {
        canAttack = false;
        yield return new WaitForSeconds(BaseAttackTime);
        canAttack = true;
    }
}
