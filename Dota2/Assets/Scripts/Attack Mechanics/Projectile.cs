﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Projectile : MonoBehaviour
{
    [HideInInspector] public UnityEvent OnProjectileHit;

    public GameObject target { get; set; }
    public GameObject source { get; set; }

    private float speed;
    private float damage;

    private void Update()
    {
        if (target != null && target.activeSelf)
            this.transform.position = Vector3.MoveTowards(this.transform.position,
                                        target.transform.position,
                                        speed * Time.deltaTime);
        else Destroy(gameObject);
        
        ////clear out if target health is 0 or is destroyed
        //if (target.GetComponent<Respawn>().respawning || target.GetComponent<Health>().CurrentHealth <= 0)
        //{
        //    Destroy(gameObject);
        //}

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == target)
        {
            target.GetComponent<Health>().Damage(damage);
            OnProjectileHit.Invoke();
            Destroy(gameObject);
        }
    }

    public void SetValues(GameObject p_target, GameObject p_source, float p_speed, float p_damage)
    {
        target = p_target;
        source = p_source;
        speed = p_speed;
        damage = p_damage;
    }
}
